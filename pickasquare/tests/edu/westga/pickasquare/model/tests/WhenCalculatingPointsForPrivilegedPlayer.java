/**
 * 
 */
package edu.westga.pickasquare.model.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.Player;
import edu.westga.pickasquare.model.PrivilegedPlayer;
import edu.westga.pickasquare.model.squares.AddPointsSquare;
import edu.westga.pickasquare.model.squares.DoublePointsSquare;
import edu.westga.pickasquare.model.squares.GoBustSquare;
import edu.westga.pickasquare.model.squares.SubtractPointsSquare;

/**
 * Testing the Privileged Player class to make sure the game
 * calculates the correct amount of points when playing the game.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class WhenCalculatingPointsForPrivilegedPlayer {
	
	private DoublePointsSquare doubleSquare;
	private AddPointsSquare addOneHundredPointsSquare;
	private GoBustSquare bustSquare;
	private SubtractPointsSquare subtractSquare;
	
	private Player player;
	
	/**
	 * No-op constructor.
	 */
	public WhenCalculatingPointsForPrivilegedPlayer() {
		// no-op
	}
	
	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp()  {
		this.addOneHundredPointsSquare = new AddPointsSquare(100);
		this.doubleSquare = new DoublePointsSquare();
		this.subtractSquare = new SubtractPointsSquare(100);
		this.bustSquare = new GoBustSquare();
		
		this.player = new PrivilegedPlayer(100);
	}

	/**
	 * Tests when you add 100 points to the privileged player's old score.
	 */
	@Test
	public void whenAddingOneHundredPointsToPrivilegedPlayer() {
		this.player.setScore(200);
		int newScore = this.addOneHundredPointsSquare.calculateNewScore(this.player.getScore());
		assertEquals(400, newScore);
	}
	
	/**
	 * Tests when you double the amount of points of the privileged player's old score.
	 */
	@Test
	public void whenDoublingPointsToPrivilegedPlayer() {
		this.player.setScore(200);
		int newScore = this.doubleSquare.calculateNewScore(this.player.getScore());
		assertEquals(600, newScore);
	}
	
	/**
	 * Tests when you add 100 points from the privileged player's old score.
	 */
	@Test
	public void whenSubtractingOneHundredPointsFromPrivilegedPlayer() {
		this.player.setScore(200);
		int newScore = this.subtractSquare.calculateNewScore(this.player.getScore());
		assertEquals(200, newScore);
	}
	
	/**
	 * Tests when a privileged player clicks on a Go-Bust square.
	 */
	@Test
	public void whenPrivilegedPlayerClicksOnGoBustSquare() {
		this.player.setScore(200);
		int newScore = this.bustSquare.calculateNewScore(this.player.getScore());
		assertEquals(0, newScore);
	}
	

}
