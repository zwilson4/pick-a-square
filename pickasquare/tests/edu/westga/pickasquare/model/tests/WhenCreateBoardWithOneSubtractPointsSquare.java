package edu.westga.pickasquare.model.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.GameBoard;
import edu.westga.pickasquare.model.OneSubtractPointsBoardFactory;

/**
 * WhenCreateBoardWithOneSubtractPointsSquare defines test cases
 * for creating a GameBoard with a OneSubtractPointsBoardFactory.
 * 
 * @author Zachary Wilson
 * @version Spring, 2015
 */
public class WhenCreateBoardWithOneSubtractPointsSquare {

	private GameBoard testBoard;
	
	/**
	 * Creates a new test object.
	 */
	public WhenCreateBoardWithOneSubtractPointsSquare() {
		// no-op
	}
	
	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp() {
		OneSubtractPointsBoardFactory testFactory = new OneSubtractPointsBoardFactory(false);
		this.testBoard = testFactory.getGameBoard();
	}

	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void boardShouldHave16Squares() {
		assertEquals(16, this.testBoard.size());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void firstSquareShouldBeBust() {
		assertEquals("Bust!", this.testBoard.getSquare(0).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void secondSquareShouldBeBust() {
		assertEquals("Bust!", this.testBoard.getSquare(1).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void thirdSquareShouldBeDouble() {
		assertEquals("Double!", this.testBoard.getSquare(2).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void forthSquareShouldBe100Points() {
		assertEquals("100", this.testBoard.getSquare(3).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void seventhSquareShouldBe100Points() {
		assertEquals("100", this.testBoard.getSquare(6).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void eighthSquareShouldBe200Points() {
		assertEquals("200", this.testBoard.getSquare(7).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void eleventhSquareShouldBe200Points() {
		assertEquals("200", this.testBoard.getSquare(10).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void twelfthSquareShouldBe200Points() {
		assertEquals("400", this.testBoard.getSquare(11).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void fifthteenthSquareShouldBe200Points() {
		assertEquals("400", this.testBoard.getSquare(14).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneSubtractPointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void sixteenthSquareShouldSubtract100Points() {
		assertEquals("-100", this.testBoard.getSquare(15).getDescription());
	}
}
