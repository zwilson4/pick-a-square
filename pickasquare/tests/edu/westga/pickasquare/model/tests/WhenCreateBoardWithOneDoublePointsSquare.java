package edu.westga.pickasquare.model.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.GameBoard;
import edu.westga.pickasquare.model.OneDoublePointsBoardFactory;

/**
 * WhenCreateBoardWithOneDoublePointsSquare defines test cases
 * for creating a GameBoard with a OneDoublePointsBoardFactory.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class WhenCreateBoardWithOneDoublePointsSquare {
	
	private GameBoard testBoard;
	
	/**
	 * Creates a new test object.
	 */
	public WhenCreateBoardWithOneDoublePointsSquare() {
		// no-op
	}

	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp()  {
		OneDoublePointsBoardFactory testFactory = new OneDoublePointsBoardFactory(false);
		this.testBoard = testFactory.getGameBoard();
	}

	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#OneDoublePointsBoardFactory(boolean)}.
	 */
	@Test
	public void boardShouldHave16Squares() {
		assertEquals(16, this.testBoard.size());
	}

	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void firstSquareShouldBeBust() {
		assertEquals("Bust!", this.testBoard.getSquare(0).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void secondSquareShouldBeBust() {
		assertEquals("Bust!", this.testBoard.getSquare(1).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void thirdSquareShouldBeDouble() {
		assertEquals("Double!", this.testBoard.getSquare(2).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void fourthSquareShouldBe100Points() {
		assertEquals("100", this.testBoard.getSquare(3).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void eigthSquareShouldBe100Points() {
		assertEquals("100", this.testBoard.getSquare(7).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void ninthSquareShouldBe200Points() {
		assertEquals("200", this.testBoard.getSquare(8).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void twelthSquareShouldBe200Points() {
		assertEquals("200", this.testBoard.getSquare(11).getDescription());
	}

	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void thirteenthSquareShouldBe400Points() {
		assertEquals("400", this.testBoard.getSquare(13).getDescription());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.OneDoublePointsBoardFactory#getGameBoard()}.
	 */
	@Test
	public void sixteenthSquareShouldBe400Points() {
		assertEquals("400", this.testBoard.getSquare(15).getDescription());
	}
}
