/**
 * 
 */
package edu.westga.pickasquare.model.squares.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.squares.SubtractPointsSquare;

/**
 * WhenSubtractPointsSquareCalculatesNewScore defines test cases
 * for an SubtractPointsSquare calculating a new score.
 * 
 * @author Zachary Wilson
 * @version Spring, 2015
 */
public class WhenSubtractPointsSquareCalculatesNewScore {
	
	private SubtractPointsSquare fiftyPointsSubtractor;
	private SubtractPointsSquare tenPointsSubtractor;
	
	/**
	 * No-op constructor.
	 */
	public WhenSubtractPointsSquareCalculatesNewScore() {
		// no-op
	}
	
	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp()  {
		this.tenPointsSubtractor = new SubtractPointsSquare(10);
		this.fiftyPointsSubtractor = new SubtractPointsSquare(50);
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.SubtractPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void newTenPointSquaresScoreShouldNotHaveBeenCalculated() {
		assertFalse(this.tenPointsSubtractor.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.SubtractPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void newFiftyPointSquaresScoreShouldNotHaveBeenCalculated() {
		assertFalse(this.fiftyPointsSubtractor.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.SubtractPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void tenPointSquareShouldSubtract10Points() {
		int newScore = this.tenPointsSubtractor.calculateNewScore(100);
		assertEquals(90, newScore);
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.SubtractPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void tenPointSquaresScoreShouldHaveBeenCalculated() {
		this.tenPointsSubtractor.calculateNewScore(100);
		assertTrue(this.tenPointsSubtractor.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.SubtractPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void fiftyPointsSquareShouldAdd50Points() {
		int newScore = this.fiftyPointsSubtractor.calculateNewScore(100);
		assertEquals(50, newScore);
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.SubtractPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void fiftyPointsSquaresScoreShouldHaveBeenCalculated() {
		this.fiftyPointsSubtractor.calculateNewScore(100);
		assertTrue(this.fiftyPointsSubtractor.scoreHasBeenCalculated());
	}
	
	
	
}
