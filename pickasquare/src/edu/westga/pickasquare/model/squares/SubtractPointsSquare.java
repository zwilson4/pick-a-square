/**
 * 
 */
package edu.westga.pickasquare.model.squares;

/**
 * SubtractPointsSquare defines a GameSquare that subtracts
 * a specified number of points to the player's score
 * when selected.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class SubtractPointsSquare extends AbstractGameSquare {
	
	private int pointsToSubtract;
	

	/**
	 * Creates a new SubtractPointsSquare instance with the
	 * specified number of points.
	 * 
	 * @precondition	pointsToSubtract > 0
	 * @postcondition	getDescription().equals("" + pointsToSubtract) &&
	 * 					!scoreHasBeenCalculated()
	 * 
	 * @param pointsToSubtract	how many points to subtract to the player's score
	 */
	public SubtractPointsSquare(int pointsToSubtract) {
		super();
		
		if (pointsToSubtract <= 0) {
			throw new IllegalArgumentException("Points to add must be > 0");
		}
		
		this.pointsToSubtract = pointsToSubtract;
		
	}

	/** 
	 * Implements the method by subtracting the points to add
	 * to the specified score.
	 * 
	 * @precondition	oldScore >= 0
	 * @postcondition	scoreHasBeenCalculated()
	 * @return oldScore - the points to subtract
	 */
	@Override
	public int calculateNewScore(int oldScore) {
		super.calculateNewScore(oldScore);
		if (oldScore < 0) {
			return 0;
		}
		return oldScore - this.pointsToSubtract;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.GameSquare#getDescription()
	 */
	@Override
	public String getDescription() {
		return "-" + this.pointsToSubtract;
	}

}
