package edu.westga.pickasquare.model.squares;


/**
 * GoBustSquare defines a GameSquare that returns
 * 0 as the new score when selected.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class GoBustSquare extends AbstractGameSquare {
	
	/**
	 * Creates a new GoBustSquare instance.
	 * 
	 * @precondition	none
	 * @postcondition	!scoreHasBeenCalculated()
	 */
	public GoBustSquare() {
		super();
	}
	
	/**  
	 * Implements the method by returning 0 for all
	 * values of oldScore.
	 * 
	 * @precondition	oldScore >= 0
	 * @postcondition	scoreHasBeenCalculated()
	 * @return 0
	 */
	@Override
	public int calculateNewScore(int oldScore) {
		super.calculateNewScore(oldScore);
		return 0;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.AbstractGameSquare#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Bust!";
	}

}
