package edu.westga.pickasquare.model;

/**
 * Player defines an interface for a player who 
 * is selecting GameSquares from the Gameboard.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 *
 */
public interface Player {

	/**
	 * Sets the player's score.
	 * 
	 * @precondition 	score >= 0
	 * @postcondition	getScore() == score
	 * 
	 * @param score the score to set
	 */
	void setScore(int score);

	/**
	 * Returns the player's score.
	 * 
	 * @precondition	none
	 * @return the score
	 */
	int getScore();

}