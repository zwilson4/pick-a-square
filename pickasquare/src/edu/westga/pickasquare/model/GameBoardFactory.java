package edu.westga.pickasquare.model;


/**
 * GameBoardFactory defines the interface for classes
 * that implement a factory that creates a GameBoard.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public interface GameBoardFactory {

	/**
	 * Returns the newly constructed GameBoard.
	 * 
	 * @precondition	none
	 * @return			the collection of GameBoard.BOARD_SIZE squares
	 */
	GameBoard getGameBoard();
}