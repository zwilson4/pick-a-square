/**
 * 
 */
package edu.westga.pickasquare.model;

/**
 * Defines an abstract base class for players on
 * the GameBoard.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 *
 */
public abstract class AbstractPlayer implements Player {
	
	private int score;
	
	/**
	 * Instantiates the data members shared by subclasses.
	 * 
	 * @precondition	none
	 * @postcondition	this.score = 0
	 */
	public AbstractPlayer() {
		this.score = 0;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.Player#setScore(int)
	 */
	@Override
	public void setScore(int score) {
		this.score = score;

	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.Player#getScore()
	 */
	@Override
	public int getScore() {
		return this.score;
	}

}
