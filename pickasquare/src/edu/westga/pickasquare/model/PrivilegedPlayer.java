/**
 * 
 */
package edu.westga.pickasquare.model;

/**
 * PrivilegedPlayer represents a person playing the pick-a-square game
 * following the privileged rules of the game.
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class PrivilegedPlayer extends AbstractPlayer {
	
	private int bonusValue;
	//private int score;

	/**
	 * Creates a new privileged player and gives the player
	 * a bonus value.
	 * 
	 * @precondition 	bonusValue != 0 
	 * 					bonusValue > 0
	 * @postcondition	the player exists 
	 * 
	 * @param bonusValue the value of the bonus.
	 */
	public PrivilegedPlayer(int bonusValue) {
		super();
		
		if (bonusValue < 0) {
			throw new IllegalArgumentException("bonus value can't be negative");
		}
		
		this.bonusValue = bonusValue;
	}
	
	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.Player#setScore(int)
	 */
	@Override
	public void setScore(int score) {
		super.setScore(score + this.getBonusValue());
	}
	
	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.Player#getScore()
	 */
	@Override
	public int getScore() {
		return super.getScore();
	}
	
	/**
	 * Returns the value of the bonus
	 * 
	 * @return the bonus value
	 */
	public int getBonusValue() {
		return this.bonusValue;
	}

	/**
	 * Sets the bonus value
	 * 
	 * @param bonusValue The bonus Value
	 */
	public void setBonusValue(int bonusValue) {
		this.bonusValue = bonusValue;
	}

}
