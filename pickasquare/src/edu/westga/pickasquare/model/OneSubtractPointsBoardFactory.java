/**
 * 
 */
package edu.westga.pickasquare.model;

import edu.westga.pickasquare.model.squares.AddPointsSquare;
import edu.westga.pickasquare.model.squares.DoublePointsSquare;
import edu.westga.pickasquare.model.squares.GoBustSquare;
import edu.westga.pickasquare.model.squares.SubtractPointsSquare;

/**
 * /**
 * OneDoublePointsBoardFactory defines a factory that
 * uses a GameBoard.Builder to create a GameBoard with
 * these squares: 
 * <ul>
 * <li> 2 GoBustSquares</li>
 * <li> 1 DoublePointsSquares</li>
 * <li> 1 SubtractPointsSquare</li>
 * <li> 4 AddPointsSquares worth 100 points</li>
 * <li> 4 AddPointsSquares worth 200 points</li>
 * <li> 4 AddPointsSquares worth 400 points</li>
 * </ul>
 * 
 * The squares are in the order described unless shuffling
 * is specified, in which case they are in random order. 
 * 
 * @author Zachary Wilson
 * @version Spring 2015
 */
public class OneSubtractPointsBoardFactory implements GameBoardFactory {

	private GameBoard.Builder theBuilder;
	
	/**
	 * Creates a new OneSubtractPointsBoardFactory instance populated
	 * with GameBoard.BOARD_SIZE GameSquare objects whose classes
	 * are as described in the class comment.
	 * 
	 * @precondition	none
	 * @postcondition	getSquares().size == GameBoard.BOARD_SIZE, &&
	 * 					the squares are shuffled iff shouldShuffle is true
	 * 
	 * @param shouldShuffle true iff the squares on the board should
		 * 					be shuffled in random order
	 */
	public OneSubtractPointsBoardFactory(boolean shouldShuffle) {
		this.theBuilder = new GameBoard.Builder(shouldShuffle);
		this.createSquares();
	}
	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.GameBoardFactory#getGameBoard()
	 */
	@Override
	public GameBoard getGameBoard() {
		return this.theBuilder.getGameBoard();
	}
	
	private void createSquares() {
		this.addTwoGoBustSquares();
		this.addOneDoubleScoreSquare();
		this.addFour100PointsSquares();
		this.addFour200PointSquares();
		this.addFour400PointSquares();	
		this.addOneSubtract100PointsSquare();
	}
	
	private void addTwoGoBustSquares() {
		this.theBuilder.addSquare(new GoBustSquare())
					   .addSquare(new GoBustSquare());
	}
	
	private void addOneDoubleScoreSquare() {
		this.theBuilder.addSquare(new DoublePointsSquare());
	}
	
	private void addFour100PointsSquares() {
		for (int i = 0; i < 4; i++) {
			this.theBuilder.addSquare(new AddPointsSquare(100));
		}
	}
	
	private void addFour200PointSquares() {
		for (int i = 0; i < 4; i++) {
			this.theBuilder.addSquare(new AddPointsSquare(200));
		}
	}
	
	private void addFour400PointSquares() {
		for (int i = 0; i < 4; i++) {
			this.theBuilder.addSquare(new AddPointsSquare(400));
		}
	}
	
	private void addOneSubtract100PointsSquare() {
		this.theBuilder.addSquare(new SubtractPointsSquare(100));
	}

}
